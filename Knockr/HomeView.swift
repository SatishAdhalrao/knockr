//
//  HomeView.swift
//  Knockr
//
//  Created by Mac on 16/04/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class HomeView: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var lblActiveUser: UILabel!
    
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    
    var itemArr = [[String: Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        lblActiveUser.text = KnockrSingletonClass.sharedInstance.activeUser
        
        var dict = [String: Any]()
        dict = ["image": "restaurants.png",
                "name": "Restaurants"]
        itemArr.append(dict)
        var dict1 = [String: Any]()
        dict1 = ["image": "bars.png",
                "name": "Bars"]
        itemArr.append(dict1)
        var dict2 = [String: Any]()
        dict2 = ["image": "Chat.png",
                 "name": "Attractions"]
        itemArr.append(dict2)
        var dict3 = [String: Any]()
        dict3 = ["image":"more.png",
                 "name": "More"]
        itemArr.append(dict3)
        
        print(itemArr)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return itemArr.count
    
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CategoriesCell
        
        let array = itemArr[indexPath.row]
        
        cell.lblCategoryName.text = (array["name"] as! String)
        
        cell.imgCategory.image = UIImage(named: array["image"] as! String)
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = collectionView.cellForItem(at: indexPath)
       
        let array = itemArr[indexPath.row]
        
        let more = (array["name"] as! String)
        
        if more == "More"
        {
            itemArr.removeAll()
            
            var dict = [String: Any]()
            dict = ["image": "restaurants.png",
                    "name": "Restaurants"]
            itemArr.append(dict)
            var dict1 = [String: Any]()
            dict1 = ["image": "bars.png",
                     "name": "Bars"]
            itemArr.append(dict1)
            var dict2 = [String: Any]()
            dict2 = ["image": "Chat.png",
                     "name": "Attractions"]
            itemArr.append(dict2)
            var dict3 = [String: Any]()
            dict3 = ["image":"more.png",
                     "name": "Sharing"]
            itemArr.append(dict3)
            var dict4 = [String: Any]()
            dict4 = ["image":"more.png",
                     "name": "Exchange"]
            itemArr.append(dict4)
            
            self.categoriesCollectionView.reloadData()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/4.0
        let yourHeight = yourWidth

        return CGSize(width: yourWidth, height: yourHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    @IBAction func btnSearch(_ sender: Any) 
    {
        let customView = Bundle.main.loadNibNamed("StartNewThreadView1", owner: nil, options: nil)![0] as! StartNewThreadView1
        
        view.addSubview(customView)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
