//
//  HomeVC.swift
//  Knockr
//
//  Created by Mac on 23/04/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Firebase

class HomeVC: UIViewController,UITabBarDelegate,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var knockrTab: UITabBar!
    
    @IBOutlet weak var lblActiveUser: UILabel!
    
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    
    @IBOutlet weak var tblTrendingTopic: UITableView!
    
    @IBOutlet weak var tblRemainingTopic: UITableView!
    
    var ref: DatabaseReference!
    
    var itemArr = [[String: Any]]()
    
    var citiesArray: NSMutableArray!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        knockrTab.delegate = self
        
        lblActiveUser.text = KnockrSingletonClass.sharedInstance.activeUser
        
        var dict = [String: Any]()
        dict = ["image": "restaurants.png",
                "name": "Restaurants"]
        itemArr.append(dict)
        var dict1 = [String: Any]()
        dict1 = ["image": "bars.png",
                 "name": "Bars"]
        itemArr.append(dict1)
        var dict2 = [String: Any]()
        dict2 = ["image": "Chat.png",
                 "name": "Attractions"]
        itemArr.append(dict2)
        var dict3 = [String: Any]()
        dict3 = ["image":"more.png",
                 "name": "More"]
        itemArr.append(dict3)
        
        print(itemArr)
        
        tblTrendingTopic.delegate = self
        tblTrendingTopic.dataSource = self

        
        
        ref = Database.database().reference().child("tbl_thread")

        ref.observe(DataEventType.value, with: { (snapshot) in
            
            for child in snapshot.children {
                let snap = child as! DataSnapshot
                let key = snap.key
                let value = snap.value
                print("key = \(key)  value = \(value!)")
                
//                self.citiesArray.add(value as Any)
                
            }
            
            print(self.citiesArray)
            
            if let objects = snapshot.children.allObjects as? [DataSnapshot] {
                print(objects)
                
                print(objects.map { $0.value })
            }
            


//            for ( key, value) in postDict
//            {
//                let dic = value as? [String : AnyObject]
//
//                if KnockrSingletonClass.sharedInstance.userID == dic!["user_id"] as! String
//                {
//
//
//
//
//
//                }
//            }
        })
    }
    
    
    // MARK: - Tableview Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1 // your number of cell here
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TrendingCell
        
        var cell: TrendingCell! = tableView .dequeueReusableCell(withIdentifier: "Cell") as? TrendingCell
        
        if (cell == nil) {
            cell = TrendingCell(style: UITableViewCell.CellStyle.default, reuseIdentifier:"Cell")
        }
        
//        cell.lblTrendingTopic.text = "12312ewd"

        return cell
        
    }
    
    private func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return itemArr.count
        
    }
    
    // MARK: - CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CategoriesCell
        
        let array = itemArr[indexPath.row]
        
        cell.lblCategoryName.text = (array["name"] as! String)
        
        cell.imgCategory.image = UIImage(named: array["image"] as! String)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = collectionView.cellForItem(at: indexPath)
        
        let array = itemArr[indexPath.row]
        
        let more = (array["name"] as! String)
        
        if more == "More"
        {
            itemArr.removeAll()
            
            var dict = [String: Any]()
            dict = ["image": "restaurants.png",
                    "name": "Restaurants"]
            itemArr.append(dict)
            var dict1 = [String: Any]()
            dict1 = ["image": "bars.png",
                     "name": "Bars"]
            itemArr.append(dict1)
            var dict2 = [String: Any]()
            dict2 = ["image": "Chat.png",
                     "name": "Attractions"]
            itemArr.append(dict2)
            var dict3 = [String: Any]()
            dict3 = ["image":"more.png",
                     "name": "Sharing"]
            itemArr.append(dict3)
            var dict4 = [String: Any]()
            dict4 = ["image":"more.png",
                     "name": "Exchange"]
            itemArr.append(dict4)
            
            self.categoriesCollectionView.reloadData()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/4.0
        let yourHeight = yourWidth
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    
    @IBAction func btnSearch(_ sender: Any)
    {
        
    }
    
    @IBAction func btnAddThread(_ sender: Any)
    {
        let customView = Bundle.main.loadNibNamed("StartNewThreadView1", owner: nil, options: nil)![0] as! StartNewThreadView1
        
        view.addSubview(customView)
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("the selected index is : \(tabBar.items!.index(of: item))")
        
        if(item.tag == 1)
        {
            //your code for tab item 1
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            if let controller = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController {
//                controller.modalPresentationStyle = .fullScreen
//                self.present(controller, animated: true, completion: nil)
//            }
        }
        else if(item.tag == 2)
        {
            let knockrRoomView = Bundle.main.loadNibNamed("UserProfileView", owner: nil, options: nil)![0] as! UserProfileView
            
            self.view.addSubview(knockrRoomView)
        }
        
    }
    

    @IBAction func btnBack(_ sender: Any)
    {
//        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
//        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        
        navigationController?.popViewController(animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
