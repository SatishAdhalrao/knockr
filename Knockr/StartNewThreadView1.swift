//
//  StartNewThreadView1.swift
//  Knockr
//
//  Created by Mac on 19/04/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Firebase
import NVActivityIndicatorView


class StartNewThreadView1: UIView,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UITextViewDelegate {
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var txtView: UITextView!
    
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var btnStartNewThread: UIButton!
    
    var selectedIndex = Int ()
    
    var categoryArray = [String]()
    
    var selectedCategory: String?
    
    var ref: DatabaseReference!
    
    var alert = UIAlertController()
    
    let  activityIndicatorView = NVActivityIndicatorView(frame:CGRect(x: (UIScreen.main.bounds.width/2)-25, y: (UIScreen.main.bounds.height/2)-25, width: 50, height: 50)
        , type: .ballRotateChase, color: UIColor(red: 103/255, green: 53/255, blue: 145/255, alpha: 1), padding: nil)
    
    override func awakeFromNib()
    {
        let yourColor : UIColor = UIColor(red: 246/255, green: 133/255, blue: 31/255, alpha: 1)
        
        txtView.delegate = self
        txtView.layer.cornerRadius = 5
        txtView.clipsToBounds = true
        txtView.layer.borderWidth = 1
        txtView.layer.borderColor = yourColor.cgColor
        
        btnStartNewThread.layer.cornerRadius = 10
        btnStartNewThread.clipsToBounds = true
        
        categoriesCollectionView.delegate = self
        categoriesCollectionView.dataSource = self
        
        categoriesCollectionView.register(UINib(nibName: "CategoriesThreadCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
        
        adjustScreenSize()
        
        txtView.text = "Start Your Topic with What, How, Why, etc"
        txtView.textColor = UIColor.lightGray
        
        lblUserName.text = KnockrSingletonClass.sharedInstance.name
        
        categoryArray = ["Sharing", "Exchange", "Hook up", "Review", "Pointless", "Discount", "Events", "Help", "Request", "Notes", "Others"]
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtView.textColor == UIColor.lightGray {
            txtView.text = nil
            txtView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtView.text.isEmpty {
            txtView.text = "Start Your Topic with What, How, Why, etc"
            txtView.textColor = UIColor.lightGray
        }
    }
    
    func adjustScreenSize() {
    
        if UIScreen.main.bounds.height <= 568 {
            self.frame = CGRect(x: 10, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height-65)
            mainView.setShadowWithCornerRadius(corners: 12)
            
            UIView.animate(withDuration: 0.3, animations: {
                self.frame = CGRect(x: 10, y: 10, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height-65)
            }) { (bool) in
                
            }
        }
        else{
            self.frame = CGRect(x: 10, y: 200, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height-180)
            mainView.setShadowWithCornerRadius(corners: 12)
            
            UIView.animate(withDuration: 0.3, animations: {
                self.frame = CGRect(x: 10, y: 200, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height-180)
            }) { (bool) in
                
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return categoryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CategoriesThreadCell
        
        cell.lblName.text = categoryArray[indexPath.row]
        
        if selectedIndex == indexPath.row
        {
            cell.lblName.backgroundColor = UIColor.green
            
            selectedCategory = cell.lblName.text
        }
        else
        {
            cell.lblName.backgroundColor = UIColor(red: 246/255, green: 133/255, blue: 31/255, alpha: 1)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        selectedIndex = indexPath.row
        
        self.categoriesCollectionView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/4.0
        let yourHeight = collectionView.bounds.height/2.0

        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    @IBAction func btnClose(_ sender: Any)
    {
        if UIScreen.main.bounds.height <= 568 {
            UIView.animate(withDuration: 0.3, animations: {
                self.frame = CGRect(x: 10, y: 10, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height-65)
            }) { (bool) in
                self.removeFromSuperview()
            }
        }
        else
        {
            UIView.animate(withDuration: 0.3, animations: {
                self.frame = CGRect(x: 10, y: 200, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height-180)
            }) { (bool) in
                self.removeFromSuperview()
            }
        }
    }
    
    
    @IBAction func btnStartNewThread(_ sender: Any)
    {
        ref = Database.database().reference()
//
//        let threadData = [
//            "user_id": KnockrSingletonClass.sharedInstance.userID,
//            "thread": txtView.text] as [String : Any]
        
        let reference = Database.database().reference().child("tbl_thread").childByAutoId()
        
        let threadData = [
            "user_id": KnockrSingletonClass.sharedInstance.userID,
            "thread": txtView.text,
            "thread_id": reference.key as Any,
            "category_name": selectedCategory as Any,
            "no_of_like_comment": 0] as [String : Any]
      
        reference.setValue(threadData)

        KnockrSingletonClass.sharedInstance.threadId = reference.key!
        print("Thread ID = ", KnockrSingletonClass.sharedInstance.threadId as Any)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Thread"), object: nil)
    }
    
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
