//
//  SignUpVC.swift
//  Knockr
//
//  Created by Mac on 20/03/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import Firebase

class SignUpVC: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,NVActivityIndicatorViewable
{

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var imagepicker = UIImagePickerController()
    var alert = UIAlertController()
    
    
    let  activityIndicatorView = NVActivityIndicatorView(frame:CGRect(x: (UIScreen.main.bounds.width/2)-25, y: (UIScreen.main.bounds.height/2)-25, width: 50, height: 50)
        , type: .ballRotateChase, color: UIColor(red: 103/255, green: 53/255, blue: 145/255, alpha: 1), padding: nil)
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        imagepicker.delegate = (self as UIImagePickerControllerDelegate & UINavigationControllerDelegate)
        
        txtName.delegate = self
        txtEmail.delegate = self
        txtPassword.delegate = self
        
//        let yourColor : UIColor = UIColor(red: 246/255, green: 133/255, blue: 31/255, alpha: 1)
        
        imgUser.layer.cornerRadius = imgUser.frame.size.width/2
        imgUser.layer.masksToBounds = true
        imgUser.clipsToBounds = true
 
//        txtName.layer.cornerRadius = 5
//        txtName.clipsToBounds = true
//        txtName.layer.borderWidth = 1
//        txtName.layer.borderColor = yourColor.cgColor
//
//        txtEmail.layer.cornerRadius = 5
//        txtEmail.clipsToBounds = true
//        txtEmail.layer.borderWidth = 1
//        txtEmail.layer.borderColor = yourColor.cgColor
//
//        txtPassword.layer.cornerRadius = 5
//        txtPassword.clipsToBounds = true
//        txtPassword.layer.borderWidth = 1
//        txtPassword.layer.borderColor = yourColor.cgColor
//
//        btnSubmit.layer.cornerRadius = 10
//        btnSubmit.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    // MARK: - UITextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    
    func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
    func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        
        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,10}")
        return passwordTest.evaluate(with: testStr)
    }
    

    @IBAction func btnSelectImage(_ sender: Any)
    {
        let alertController = UIAlertController(title: "Choose from:", message: "", preferredStyle: .actionSheet)
        
        let sendButton = UIAlertAction(title: "Gallery", style: .default, handler: { (action) -> Void in
            self.galleryimage()
        })
        
        let  deleteButton = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            self.cameraimage()
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        
        
        alertController.addAction(sendButton)
        alertController.addAction(deleteButton)
        alertController.addAction(cancelButton)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func galleryimage()
    {
        imagepicker.allowsEditing = false
        imagepicker.sourceType = .photoLibrary
        imagepicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(imagepicker, animated: true, completion: nil)
    }
    
    func cameraimage()
    {
        imagepicker.allowsEditing = false
        imagepicker.sourceType = UIImagePickerController.SourceType.camera
        imagepicker.cameraCaptureMode = .photo
        imagepicker.modalPresentationStyle = .fullScreen
        present(imagepicker,animated: true,completion: nil)
    }
    
    
    // MARK: - ImagePicker Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        guard let image = info[UIImagePickerController.InfoKey.originalImage]
            as? UIImage else
        {
                return
        }
        
        imgUser.image = image
        
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("apple.png")
        print(paths)
        let imageData = image.pngData()
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        
        dismiss(animated:true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnSubmit(_ sender: Any)
    {
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
        
        var userName = txtName.text
        userName = userName?.trimmingCharacters(in: .whitespacesAndNewlines)
        var emailstring = txtEmail.text
        emailstring = emailstring?.trimmingCharacters(in: .whitespacesAndNewlines)
        var password = txtPassword.text
        password = password?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if userName?.characters.count==0
        {
            alerttoshow(msg: "Please Enter Username")
        }
        else if emailstring?.characters.count==0
        {
            alerttoshow(msg: "Please Enter Email")
        }
        else if validateEmail(candidate: emailstring!) == false
        {
            alerttoshow(msg: "Please Enter Valid Email Id")
        }
        else if isValidPassword(testStr: password!) == false
        {
            alerttoshow(msg: "Password must be between 6 to 10 characters which contain at least one numeric digit, one uppercase, and one lowercase letter")
        }
        else
        {
            let fm = FileManager.default
            let docsurl = try! fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let myurl = docsurl.appendingPathComponent("apple.png")
            
            let image = UIImage(contentsOfFile: myurl.path)
            // Do whatever you want with the image
            
            
            let imageData = imgUser.image!.pngData()
            
//            let imageData = imgUser.image!.jpegData(compressionQuality:  1.0)
            
            
            let parameters: [String: Any] = ["display_name":userName!,"username":emailstring!,"password":password!]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                if let data = imageData
                {
                    multipartFormData.append(data, withName: "profile_picture", fileName: "file.png", mimeType: "image/jpg")
                }
                
            }, usingThreshold: UInt64.init(), to: "http://innolytic.guru/knockr/Webs/sign_up?", method: .post, headers: nil) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        if let responseData = response.data
                        {
                            do
                            {
                                let myjson = try JSON(data: responseData)
                                print(myjson)
                                
                                let errorcode = myjson["success"]
                                print(errorcode)
                                if errorcode == "User registration successfully done."
                                {
                                    self.activityIndicatorView.stopAnimating()
                                    let alertController = UIAlertController(title: "OTP Sent", message: "", preferredStyle: .alert)
                                    let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                                        let oTPVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPView") as! OTPView
                                        self.navigationController!.pushViewController(oTPVC, animated: true)
                                        
                                        
                                    }
                                    alertController.addAction(action1)
                                    self.present(alertController, animated: true, completion: nil)
                                    
                                    
//                                    let message = try JSONSerialization.jsonObject(with: responseData, options:.mutableContainers)
//                                    let jsonResult = message as? NSMutableDictionary
//                                    print(jsonResult as Any)
//                                    
//                                    let imageDictionary = jsonResult!["user_data"] as! [String : String]
//                                    print(imageDictionary)
//                                    
//                                    
//                                    
                                    let userData = myjson["user_data"]
                                    print(userData)
                                    
                                    let userID = userData["user_id"].string
                                    print(userID as Any)
                                    
                                    KnockrSingletonClass.sharedInstance.userID = userID!
                                    
                                    let userName = userData["username"].string
                                    print(userName as Any)
                                    
                                    KnockrSingletonClass.sharedInstance.userName = userName!
//
//                                    self.ref.child("tbl_user").child(userID!).setValue(imageDictionary)

                                    
                                }
                                
                                let errorUsername = myjson["username_error"]
                                print(errorUsername)
                                if errorUsername == "Username already exist."
                                {
                                    self.alerttoshow(msg: "Username already exist.")
                                }
                                
                            }
                            catch
                            {
                                self.activityIndicatorView.stopAnimating()
                                self.alerttoshow(msg: "Server issue")
                                print(error)
                                
                            }
                            
                        }
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                }
                
            }
        }
        
    }
    
    func alerttoshow(msg:String)
    {
        alert = UIAlertController (title: msg, message: nil, preferredStyle: UIAlertController.Style.alert)
        perform(#selector(dismissalert), with: nil, afterDelay: 2)
            self.present(alert, animated: true, completion: nil)
        
    }
        
    @objc func dismissalert()
    {
        self.activityIndicatorView.stopAnimating()
            
        alert.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


}
