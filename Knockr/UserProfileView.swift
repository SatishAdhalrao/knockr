//
//  UserProfileView.swift
//  Knockr
//
//  Created by Mac on 23/04/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit

class UserProfileView: UIView {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    
    override func awakeFromNib()
    {
        
        lblUserName.text = KnockrSingletonClass.sharedInstance.name
        
        adjustScreenSize()
        
    }
    
    func adjustScreenSize() {
        if UIScreen.main.bounds.height <= 568 {
            self.frame = CGRect(x: 10, y: 30, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height-95)
            mainView.setShadowWithCornerRadius(corners: 12)
            
            UIView.animate(withDuration: 0.3, animations: {
                self.frame = CGRect(x: 10, y: 30, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height-95)
            }) { (bool) in
                
            }
        }
        else{
            
            self.frame = CGRect(x: 10, y: 200, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height-320)
            mainView.setShadowWithCornerRadius(corners: 12)
            
            UIView.animate(withDuration: 0.3, animations: {
                self.frame = CGRect(x: 10, y: 200, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height-320)
            }) { (bool) in
                
            }
        }
    }
    
    @IBAction func btnClose(_ sender: Any)
    {
        if UIScreen.main.bounds.height <= 568 {
            UIView.animate(withDuration: 0.3, animations: {
                self.frame = CGRect(x: 10, y: 30, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height-95)
            }) { (bool) in
                self.removeFromSuperview()
            }
        }
        else
        {
            UIView.animate(withDuration: 0.3, animations: {
                self.frame = CGRect(x: 10, y: 200, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height-320)
            }) { (bool) in
                self.removeFromSuperview()
            }
        }
    }
    
    @IBAction func btnLogout(_ sender: Any)
    {
       NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Logout"), object: nil)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    
}
