//
//  SideMenuView.swift
//  Knockr
//
//  Created by Mac on 11/04/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit

class SideMenuView: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var tblSideMenu: UITableView!
    
    var alert = UIAlertController()
    
    var menuArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblSideMenu.delegate = self
        tblSideMenu.dataSource = self
        
        imgProfile.layer.cornerRadius = imgProfile.frame.size.width/2
        imgProfile.layer.masksToBounds = true
        imgProfile.clipsToBounds = true
        
        DispatchQueue.main.async  {
            let url = NSURL(string:KnockrSingletonClass.sharedInstance.profileUrl)
            let data = NSData(contentsOf:url! as URL)
            
            // It is the best way to manage nil issue.
            if data!.length > 0 {
                self.imgProfile.image = UIImage(data:data! as Data)
            } else {
                // In this when data is nil or empty then we can assign a placeholder image
                self.imgProfile.image = UIImage(named: "user.png")
            }
            
        }
        
        lblName.text = KnockrSingletonClass.sharedInstance.name
        
        menuArray = ["zero"]
    }
    
    
    // MARK: - Tableview Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menuArray.count // your number of cell here
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "Zero") as! UITableViewCell
        //  cell?.textLabel!.text = objectsArray[indexPath.section].sectionObjects[indexPath.row]
    
        return cell
        
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            let alertController = UIAlertController(title: "Alert", message: "Do you want to Logout..?", preferredStyle: .alert)
            let action1 = UIAlertAction(title: "Cancel", style: .default) { (action:UIAlertAction) in
                
            }
            
            let action2 = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction) in
                
                if KnockrSingletonClass.sharedInstance.loginUsing == "normalLogin"
                {
                    UserDefaults.standard.setValue(nil, forKey: "userID")
                    
                    let next = self.storyboard?.instantiateViewController(withIdentifier: "MainView")
                    self.present(next!, animated: true, completion: nil)
                    
                    print("Logout")
                }
                else if KnockrSingletonClass.sharedInstance.loginUsing == "googleLogin"
                {
                    do {
                        try Auth.auth().signOut()
                        GIDSignIn.sharedInstance().signOut()
                        
                        UserDefaults.standard.setValue(nil, forKey: "userID")
                        
                        let next = self.storyboard?.instantiateViewController(withIdentifier: "MainView")
                        self.present(next!, animated: true, completion: nil)
                        
                        print("Logout")
                    } catch let err {
                        print(err)
                    }
                    
                }
                else if KnockrSingletonClass.sharedInstance.loginUsing == "facebookLogin"
                {
                    FBSDKLoginManager().logOut()
                    
                    try!  Auth.auth().signOut()
                    
                    UserDefaults.standard.setValue(nil, forKey: "userID")
                    
                    let next = self.storyboard?.instantiateViewController(withIdentifier: "MainView")
                    self.present(next!, animated: true, completion: nil)
                    
                    print("Logout")
                }
            }
            
            alertController.addAction(action1)
            alertController.addAction(action2)
            
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
