//
//  ViewController.swift
//  Knockr
//
//  Created by Mac on 15/03/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit



class ViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var sideMenu: UIButton!
    
    var alert = UIAlertController()
    
    var userPosition = CLLocationCoordinate2D()
    var locationManager: CLLocationManager = CLLocationManager()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        mapView.delegate = self
        mapView.mapType = GMSMapViewType.normal
        
        //showMarker(position: camera.target)
        
        /*
        let circleCenter : CLLocationCoordinate2D  = CLLocationCoordinate2DMake(18.5204, 73.8567);
        let circ = GMSCircle(position: circleCenter, radius: 100)
        circ.fillColor = UIColor(red: 0.25, green: 0, blue: 0, alpha: 0.05);
        circ.strokeColor = .black
        circ.strokeWidth = 2.5;
        circ.map = mapView;
 */
        
        mapView.bringSubviewToFront(sideMenu)
    
        
        if self.revealViewController() != nil {
            
//            button1.target = self.revealViewController()
//
//            button1.action = #selector(SWRevealViewController.revealToggle(_:))

            sideMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)

            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(KnockTheDoor), name:Notification.Name(rawValue: "KnockTheDoor"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(Logout), name:Notification.Name(rawValue: "Logout"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(Thread), name:Notification.Name(rawValue: "Thread"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
//        NotificationCenter.default.addObserver(self, selector: #selector(KnockTheDoor), name:Notification.Name(rawValue: "KnockTheDoor"), object: nil)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(Logout), name:Notification.Name(rawValue: "Logout"), object: nil)
//
//        NotificationCenter.default.addObserver(self, selector: #selector(Thread), name:Notification.Name(rawValue: "Thread"), object: nil)
    }
    
    @objc func KnockTheDoor() {
        
        DispatchQueue.main.async(execute: {
            
            let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            self.navigationController!.pushViewController(mainVC, animated: true)
//            self.present(mainVC, animated: true, completion: {
            
//            })
            
        })
    }
    
    @objc func Thread() {
        
        DispatchQueue.main.async(execute: {
            
            let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "ThreadVC") as! ThreadVC
            self.navigationController!.pushViewController(mainVC, animated: true)
            
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        //get current user location for startup
        // if CLLocationManager.locationServicesEnabled() {
        // }
        initializeTheLocationManager()
    }
    
    
    func initializeTheLocationManager() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        setUserLocationAndCamera()
        updateUserLocationToFirebase()
    }
    
    func setUserLocationAndCamera() {
        
        mapView.clear()
        userPosition = CLLocationCoordinate2D(latitude: locationManager.location?.coordinate.latitude ?? 0.0, longitude: locationManager.location?.coordinate.longitude ?? 0.0)
        //        let marker = GMSMarker(position: userPosition)
        //        marker.icon = UIImage(named: "ic_user")
        //        marker.map = mapView
        
        mapView.isMyLocationEnabled = true
        
        let camera = GMSCameraPosition.camera(withLatitude: userPosition.latitude, longitude: userPosition.longitude, zoom: 15.0)
        mapView.camera = camera
        getKnockerRoomDataFromFirebase()
    }
    
    func getKnockerRoomDataFromFirebase() {
        
        var ref: DatabaseReference!
        
        ref = Database.database().reference().child("tbl_rooms")
        
        ref.observe(DataEventType.value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            
            self.showMarkerWithInfo(postDict)
            
            /*
             for (key, value) in postDict {
             print("\(key) -> \(value)")
             if let dic = value as? [String : AnyObject] {
             self.showMarkerWithInfo(dic)
             }
             }*/
            
        })
    }
    
    func updateUserLocationToFirebase() {
        
        var ref: DatabaseReference!
        
        ref = Database.database().reference()
        
        let userID = KnockrSingletonClass.sharedInstance.userID
        
        let dictLocation = NSMutableDictionary()
        dictLocation.setValue("\(locationManager.location?.coordinate.latitude ?? 0.0)", forKey: "latitude")
        dictLocation.setValue("\(locationManager.location?.coordinate.longitude ?? 0.0)", forKey: "longitude")
        
        ref.child("tbl_user_location").child(userID).updateChildValues(dictLocation as! [AnyHashable : Any])
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            setUserLocationAndCamera()
            break
        case .authorizedAlways:
            // If always authorized
            setUserLocationAndCamera()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            break
        default:
            break
        }
    }
    
    
    func showMarker(position: CLLocationCoordinate2D){
        let marker = GMSMarker()
        marker.position = position
        marker.title = "Palo Alto"
        marker.snippet = "San Francisco"
        marker.map = mapView
    }
    
    func showMarkerWithInfo(_ finalDic: [String : AnyObject]){
        
        var bounds = GMSCoordinateBounds()
        
        for ( _, value) in finalDic {
            
            if let dic = value as? [String : AnyObject] {
                
                let marker = GMSMarker()
                if let center_latitude = dic["center_latitude"] as? String, let center_longitude = dic["center_longitude"] as? String {
                    
                    if let doubleLat = Double(center_latitude), let doubleLong = Double(center_longitude) {
                        
                        let coordinateMarker = CLLocation(latitude: doubleLat, longitude: doubleLong)
                        
                        var distanceInMeters = Double()
                        distanceInMeters = locationManager.location?.distance(from: coordinateMarker) ?? 0.0 // result is in meters
                        
                        // Bhargav Comment : please uncomment it
                             if distanceInMeters <= 3000.0 {
                        let location:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: doubleLat, longitude: doubleLong)
                        marker.position = location
                        bounds = bounds.includingCoordinate(location)
                        
                        if let roomName = dic["room_name"] as? String {
                            marker.title = roomName
                        }
                        if let address = dic["address"] as? String {
                            marker.snippet = address
                        }
                        marker.map = mapView
                        var markerImage = UIImage()
                        if distanceInMeters <= 100.0 {
                            markerImage = UIImage(named: "door-open")!.withRenderingMode(.alwaysOriginal)
                        }
                        else {
                            markerImage = UIImage(named: "door-close")!.withRenderingMode(.alwaysOriginal)
                        }
                        
                        let markerView = UIImageView(image: markerImage)
                        marker.iconView = markerView
                        
                        let circleCenter : CLLocationCoordinate2D = CLLocationCoordinate2DMake(doubleLat,doubleLong)
                        let circ = GMSCircle(position: circleCenter, radius: 100)
                        circ.fillColor = UIColor(red: 244.0/255.0, green: 213.0/255.0, blue: 184/255.0, alpha: 1.0)
                        circ.strokeColor = UIColor(red: 244.0/255.0, green: 213.0/255.0, blue: 184/255.0, alpha: 1.0)
                        circ.strokeWidth = 1
                        circ.map = mapView
                        circ.isTappable = true
                        circ.userData = dic as? [String : AnyObject]
                                }
                    }else{
                        print("Could not get proper latitude and longitude values")
                    }
                }else{
                    print("Could not get proper latitude and longitude values")
                }
            }
        }
        
        //  let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
        //  self.mapView.animate(with: update)
    }
    
    func mapView(mapView: GMSMapView, didTapOverlay overlay: GMSOverlay) {
        /* Here we cast the GMSOverlay to be a GMSPolygon */
        let overlay = overlay as! GMSPolygon
        
        overlay.fillColor = UIColor.red
        
        /* Now the color of the polygon has changed on the map */
    }
    
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        
        let knockrRoomView = Bundle.main.loadNibNamed("KnockrRoomView", owner: nil, options: nil)![0] as! KnockrRoomView
        
        if let dict = overlay.userData as? [String : AnyObject] {
            knockrRoomView.KnockrDetail = dict
        }
        self.view.addSubview(knockrRoomView)
    }
    
    @objc func Logout()
    {
        let alertController = UIAlertController(title: "Alert", message: "Do you want to Logout..?", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Cancel", style: .default) { (action:UIAlertAction) in
            
        }
        
        let action2 = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction) in
            
            if KnockrSingletonClass.sharedInstance.loginUsing == "normalLogin"
            {
                UserDefaults.standard.setValue(nil, forKey: "userID")
                
                let next = self.storyboard?.instantiateViewController(withIdentifier: "MainView")
                self.present(next!, animated: true, completion: nil)
                
                print("Logout")
            }
            else if KnockrSingletonClass.sharedInstance.loginUsing == "googleLogin"
            {
                do {
                    try Auth.auth().signOut()
                    GIDSignIn.sharedInstance().signOut()
                    
                    UserDefaults.standard.setValue(nil, forKey: "userID")
                    
                    let next = self.storyboard?.instantiateViewController(withIdentifier: "MainView")
                    self.present(next!, animated: true, completion: nil)
                    
                    print("Logout")
                } catch let err {
                    print(err)
                }
                
            }
            else if KnockrSingletonClass.sharedInstance.loginUsing == "facebookLogin"
            {
                FBSDKLoginManager().logOut()
                
                try!  Auth.auth().signOut()
                
                UserDefaults.standard.setValue(nil, forKey: "userID")
                
                let next = self.storyboard?.instantiateViewController(withIdentifier: "MainView")
                self.present(next!, animated: true, completion: nil)
                
                print("Logout")
            }
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func alerttoshow(msg:String)
    {
        alert = UIAlertController (title: msg, message: nil, preferredStyle: UIAlertController.Style.alert)
        perform(#selector(dismissalert), with: nil, afterDelay: 2)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func dismissalert()
    {
//        self.activityIndicatorView.stopAnimating()
        
        alert.dismiss(animated: true, completion: nil)
    }
}
