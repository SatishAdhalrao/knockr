//
//  LoginVC.swift
//  Knockr
//
//  Created by Mac on 19/03/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Alamofire
import Gloss
import SwiftyJSON
import NVActivityIndicatorView
import Firebase
import SkyFloatingLabelTextField


class LoginVC: UIViewController,UITextFieldDelegate,NVActivityIndicatorViewable {

   
   
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var txtUserName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    
    var alert = UIAlertController()
    
    let  activityIndicatorView = NVActivityIndicatorView(frame:CGRect(x: (UIScreen.main.bounds.width/2)-25, y: (UIScreen.main.bounds.height/2)-25, width: 50, height: 50)
        , type: .ballRotateChase, color: UIColor(red: 103/255, green: 53/255, blue: 145/255, alpha: 1), padding: nil)
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
 

        
//        txtUserName.text = "abcc@gmail.com"
//        txtPassword.text = "123456"
    
        txtUserName.delegate = self
        txtPassword.delegate = self
        
        txtUserName.title = "Email" 
        txtPassword.title = "Password"
        
        
//        let yourColor : UIColor = UIColor(red: 246/255, green: 133/255, blue: 31/255, alpha: 1)
//        
//        txtUserName.layer.cornerRadius = 5
//        txtUserName.clipsToBounds = true
//        txtUserName.layer.borderWidth = 1
//        txtUserName.layer.borderColor = yourColor.cgColor
//        
//        txtPassword.layer.cornerRadius = 5
//        txtPassword.clipsToBounds = true
//        txtPassword.layer.borderWidth = 1
//        txtPassword.layer.borderColor = yourColor.cgColor
//        
//        btnLogin.layer.cornerRadius = 10
//        btnLogin.clipsToBounds = true
        
    
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    // MARK: - UITextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    
    @IBAction func btnLogin(_ sender: Any)
    {
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
        var userName = txtUserName.text
        userName = userName?.trimmingCharacters(in: .whitespacesAndNewlines)
        var password = txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)

        if userName?.characters.count==0
        {
            alerttoshow(msg: "Please Enter Username")
        }
        else if password?.characters.count==0
        {
            alerttoshow(msg: "Please Enter Valid Password")
            
        }
        else
        {
            let urlString = URL(string: "http://innolytic.guru/knockr/Webs/"+"login?")
    
            let params: [String: Any] = ["username":userName!, "password":password!]
            
            print(params)
            
            Alamofire.request(urlString!, method: .post, parameters: params,  headers: nil).responseJSON { (response) in
                
                
                guard (response.error == nil) else
                {
                    self.activityIndicatorView.stopAnimating()
//                    let alertController = UIAlertController(title: "Error", message: response.error!.localizedDescription , preferredStyle: .alert)
//                    let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
//
//                    }
//                    alertController.addAction(action1)
//                    self.present(alertController, animated: true, completion: nil)
                    
                    self.alerttoshow(msg: "Somthing Went Wrong")
                    
                    return
                }
                
                if let responseData = response.data
                {
                    do
                    {
                        let myjson = try JSON(data: responseData)
                        print(myjson)
                        
                        let errorcode = myjson["success"]
                        print(errorcode)
                        if errorcode == "Login success."
                        {
                            self.activityIndicatorView.stopAnimating()
                            let alertController = UIAlertController(title: "Login Sucessfully", message: "", preferredStyle: .alert)
                            let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                                
                                let userData = myjson["user_data"]
                                print(userData)
                                
                                let userID = userData["user_id"].string
                                print(userID as Any)
                                
                                KnockrSingletonClass.sharedInstance.loginUsing = "normalLogin"
                                UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.loginUsing, forKey: "loginUsing")

                                
                                KnockrSingletonClass.sharedInstance.userID = userID!
                                
                                UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.userID, forKey: "userID")
                                
                                let userName = userData["username"].string
                                print(userName as Any)
                                
                                KnockrSingletonClass.sharedInstance.userName = userName!

                                UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.userName, forKey: "userName")
                                
                                
                                let name = userData["display_name"].string
                                print(name as Any)
                                
                                KnockrSingletonClass.sharedInstance.name = name!
                                UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.name, forKey: "Name")
                                
                                let profileUrl = userData["profile_picture"].string
                                
                                KnockrSingletonClass.sharedInstance.profileUrl = profileUrl!
                                UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.profileUrl, forKey: "profileUrl")
                                
                                
                                UserDefaults.standard.synchronize()
                                
                                let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                self.navigationController!.pushViewController(secondViewController, animated: true)
                            }
                            alertController.addAction(action1)
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                        else 
                        {
                            self.alerttoshow(msg: "Somthing Went Wrong")
                        }
                        
                    }
                    catch
                    {
                        self.activityIndicatorView.stopAnimating()
                        self.alerttoshow(msg: "Server issue")
                        print(error)
                        
                    }
                    
                }
            }
        }
    }
    
    
    func alerttoshow(msg:String)
    {
        alert = UIAlertController (title: msg, message: nil, preferredStyle: UIAlertController.Style.alert)
        perform(#selector(dismissalert), with: nil, afterDelay: 2)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func dismissalert()
    {
        self.activityIndicatorView.stopAnimating()
        
        alert.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
