//
//  ThreadCell.swift
//  Knockr
//
//  Created by Mac on 25/04/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class ThreadCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblThreadComment: UILabel!
    @IBOutlet weak var btnLikeToComment: UIButton!
    @IBOutlet weak var btnReplyToComment: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
