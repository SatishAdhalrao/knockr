//
//  Utility.swift
//  Knockr
//
//  Created by Sandeep Jain on 4/14/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
let yourColor : UIColor = UIColor(red: 246/255, green: 133/255, blue: 31/255, alpha: 1)

extension UITextField {
    
    func useUnderline() {
        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        border.borderColor = UIColor.gray.withAlphaComponent(0.6).cgColor
        border.frame = CGRect(origin: CGPoint(x: 0,y :self.frame.size.height - borderWidth), size: CGSize(width: self.frame.size.width + 40, height: self.frame.size.height))
        border.borderWidth = borderWidth
        self.textColor = yourColor
//        self.attributedPlaceholder = NSAttributedString(string: "placeholder text",
//                                                               attributes: [NSAttributedString.Key.foregroundColor: yourColor.withAlphaComponent(0.5)])
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
