//
//  OTPView.swift
//  Knockr
//
//  Created by Mac on 29/03/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import SVPinView
import Alamofire
import Gloss
import SwiftyJSON
import NVActivityIndicatorView
import Firebase


class OTPView: UIViewController {
    
    
    @IBOutlet weak var pinView: SVPinView!
    
    @IBOutlet weak var submit: UIButton!
    
    var otp = ""
    var alert = UIAlertController()
    var ref: DatabaseReference!
    
    let  activityIndicatorView = NVActivityIndicatorView(frame:CGRect(x: (UIScreen.main.bounds.width/2)-25, y: (UIScreen.main.bounds.height/2)-25, width: 50, height: 50)
        , type: .ballRotateChase, color: UIColor(red: 103/255, green: 53/255, blue: 145/255, alpha: 1), padding: nil)
   
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        ref = Database.database().reference()
        
        configurePinView()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func configurePinView() {
        
        pinView.pinLength = 4
        pinView.secureCharacter = "\u{25CF}"
        pinView.interSpace = 10
        pinView.textColor = UIColor.white
        pinView.borderLineColor = UIColor.white
        pinView.activeBorderLineColor = UIColor.white
        pinView.borderLineThickness = 1
        pinView.shouldSecureText = true
        pinView.allowsWhitespaces = false
        pinView.style = .none
        pinView.fieldBackgroundColor = UIColor.orange.withAlphaComponent(0.3)
        pinView.activeFieldBackgroundColor = UIColor.orange.withAlphaComponent(0.5)
        pinView.fieldCornerRadius = 15
        pinView.activeFieldCornerRadius = 15
        pinView.placeholder = "******"
        pinView.becomeFirstResponderAtIndex = 0
        
        pinView.font = UIFont.systemFont(ofSize: 15)
        pinView.keyboardType = .phonePad
        pinView.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        
        pinView.didFinishCallback = didFinishEnteringPin(pin:)
        pinView.didChangeCallback = { pin in
            print("The entered pin is \(pin)")
            
//            otp = pin
        }
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    func didFinishEnteringPin(pin:String) {
//        showAlert(title: "Success", message: "The Pin entered is \(pin)")
        
        otp = pin
    }
    
    // MARK: Helper Functions
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setGradientBackground(view:UIView, colorTop:UIColor, colorBottom:UIColor) {
        for layer in view.layer.sublayers! {
            if layer.name == "gradientLayer" {
                layer.removeFromSuperlayer()
            }
        }
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop.cgColor, colorBottom.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        gradientLayer.name = "gradientLayer"
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    

    @IBAction func btnSubmit(_ sender: UIButton)
    {
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
        let urlString = URL(string: "http://innolytic.guru/knockr/Webs/"+"verify_otp?")
        
        let params: [String: Any] = ["user_id":KnockrSingletonClass.sharedInstance.userID, "otp":otp]
        
        print(params)
        
        Alamofire.request(urlString!, method: .post, parameters: params,  headers: nil).responseJSON { (response) in
            
            
            guard (response.error == nil) else
            {
                self.activityIndicatorView.stopAnimating()
                let alertController = UIAlertController(title: "Error", message: response.error!.localizedDescription , preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                return
            }
            
            if let responseData = response.data
            {
                do
                {
                    let myjson = try JSON(data: responseData)
                    print(myjson)
                    
                    let errorcode = myjson["success"]
                    print(errorcode)
                    if errorcode == "User Account Verified Successfully."
                    {
                        self.activityIndicatorView.stopAnimating()
                        let alertController = UIAlertController(title: "User registration successfully done.", message: "", preferredStyle: .alert)
                        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                            self.navigationController!.pushViewController(loginVC, animated: true)
                        }
                        alertController.addAction(action1)
                        self.present(alertController, animated: true, completion: nil)
                        
                        // Send User Data To Firebase
                        
                        let jsonDict = response.result.value as? [String:Any]
                        
                        let dictUserData = jsonDict!["user_data"] as? [String:Any]
                        
                        let userID = dictUserData!["user_id"] as? String
                        print(userID as Any)
                    
                        self.ref.child("tbl_user").child(userID!).updateChildValues(dictUserData!)
                        
                    }
                    else
                    {
                        self.alerttoshow(msg: "Somthing Went Wrong")
                    }
                    
                }
                catch
                {
                    self.activityIndicatorView.stopAnimating()
                    self.alerttoshow(msg: "Server issue")
                    print(error)
                    
                }
                
            }
        }
        
    }
    
    
    @IBAction func btnResendOTP(_ sender: Any)
    {
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
        let urlString = URL(string: "http://innolytic.guru/knockr/Webs/"+"resend_otp?")
        
        let params: [String: Any] = ["username":KnockrSingletonClass.sharedInstance.userName]
        
        print(params)
        
        Alamofire.request(urlString!, method: .post, parameters: params,  headers: nil).responseJSON { (response) in
            
            
            guard (response.error == nil) else
            {
                self.activityIndicatorView.stopAnimating()
                let alertController = UIAlertController(title: "Error", message: response.error!.localizedDescription , preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                    
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                return
            }
            
            if let responseData = response.data
            {
                do
                {
                    let myjson = try JSON(data: responseData)
                    print(myjson)
                    
                    let errorcode = myjson["success"]
                    print(errorcode)
                    if errorcode == "OTP Successfully Send."
                    {
                        self.activityIndicatorView.stopAnimating()
                        let alertController = UIAlertController(title: "OTP Successfully Send.", message: "", preferredStyle: .alert)
                        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.navigationController!.pushViewController(secondViewController, animated: true)
                        }
                        alertController.addAction(action1)
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    else
                    {
                        self.alerttoshow(msg: "Somthing Went Wrong")
                    }
                    
                }
                catch
                {
                    self.activityIndicatorView.stopAnimating()
                    self.alerttoshow(msg: "Server issue")
                    print(error)
                    
                }
                
            }
        }
    }
    
    func alerttoshow(msg:String)
    {
        alert = UIAlertController (title: msg, message: nil, preferredStyle: UIAlertController.Style.alert)
        perform(#selector(dismissalert), with: nil, afterDelay: 2)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func dismissalert()
    {
        self.activityIndicatorView.stopAnimating()
        
        alert.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
