//
//  CategoriesCell.swift
//  Knockr
//
//  Created by Mac on 17/04/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class CategoriesCell: UICollectionViewCell {
    
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblCategoryName: UILabel!
}
