//
//  KnockrSingletonClass.swift
//  Knockr
//
//  Created by Mac on 29/03/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class KnockrSingletonClass: NSObject
{
    class var sharedInstance:KnockrSingletonClass
    {
        struct Singleton
        {
            static let instance = KnockrSingletonClass()
        }
        return Singleton.instance
    }
    
    // MARK: - LoginView
    var userID: String = ""
    var userName: String = ""
    var name: String = ""
    var loginUsing: String = ""
    var profileUrl: String = ""
    var activeUser: String = ""
    var threadId: String = ""
    
    
}
