//
//  StartView.swift
//  Knockr
//
//  Created by Mac on 05/04/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class StartView: UIViewController {

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let userID = UserDefaults.standard.value(forKey: "userID")
        {
            KnockrSingletonClass.sharedInstance.userID = userID as! String
        }
        
        if let loginUsing = UserDefaults.standard.value(forKey: "loginUsing")
        {
            KnockrSingletonClass.sharedInstance.loginUsing = loginUsing as! String
        }
        
        if let profileUrl = UserDefaults.standard.value(forKey: "profileUrl")
        {
            KnockrSingletonClass.sharedInstance.profileUrl = profileUrl as! String
        }
        
        if let name = UserDefaults.standard.value(forKey: "Name")
        {
            KnockrSingletonClass.sharedInstance.name = name as! String
        }
    
        perform(#selector(goToNextView), with: nil, afterDelay: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @objc func goToNextView()
    {
        if KnockrSingletonClass.sharedInstance.userID .isEmpty
        {
            let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "MainView") as! MainView
            self.navigationController!.pushViewController(mainVC, animated: true)
        }
        else
        {
            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.navigationController!.pushViewController(secondViewController, animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
