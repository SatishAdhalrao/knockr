//
//  ThreadVC.swift
//  Knockr
//
//  Created by Mac on 25/04/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Firebase

class ThreadVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {
    

    @IBOutlet weak var lblThread: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var txtViewThreadComment: UITextView!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var tblThradView: UITableView!
    
    var ref: DatabaseReference!
    
    var arrayComments = [[String: String]]()
    
    var arrayLikes = [[String: String]]()
    
    var addCount: Int = 0
    
    var noOfCommentLike: Int = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        txtViewThreadComment.delegate = self
        txtViewThreadComment.text = "Write a Comment..."
        txtViewThreadComment.textColor = UIColor.lightGray
        
        tblThradView.delegate = self
        tblThradView.dataSource = self
        
        ref = Database.database().reference().child("tbl_thread")
        
        ref.observe(DataEventType.value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            
            let dict = postDict[KnockrSingletonClass.sharedInstance.threadId] as? [String : AnyObject] ?? [:]
            print("Thread1 =",dict as Any)
            
            self.lblThread.text = (dict["thread"] as! String)
        })
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        ref = Database.database().reference().child("tbl_thread_comment").child(KnockrSingletonClass.sharedInstance.threadId)
        
        ref.observe(DataEventType.value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            
            print("Data = ",postDict)
            
            self.arrayComments.removeAll()
            
            for ( _, value) in postDict
            {
                let dic = value as? [String : AnyObject]
    
                self.arrayComments.append(dic! as! [String : String])
                
                self.lblComment.text = String(self.arrayComments.count) + " " + "Comments"
                
                self.btnComment.setTitle(" " + self.lblComment.text!, for: UIControl.State.normal)
                
                self.tblThradView .reloadData()
            }
        })
        
    }
    
    // MARK: - TextView Delegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtViewThreadComment.textColor == UIColor.lightGray {
            txtViewThreadComment.text = nil
            txtViewThreadComment.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtViewThreadComment.text.isEmpty {
            txtViewThreadComment.text = "Write a Comment..."
            txtViewThreadComment.textColor = UIColor.lightGray
        }
    }
    
    
    // MARK: - Tableview Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayComments.count // your number of cell here
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ThreadCell
        //  cell?.textLabel!.text = objectsArray[indexPath.section].sectionObjects[indexPath.row]
        
        let array = arrayComments[indexPath.row]
        
        cell.lblName.text = array["user_name"]
        cell.lblThreadComment.text = array["thread_comment"]
        return cell
        
    }
    
    private func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    
    @IBAction func btnLike(_ sender: Any)
    {
        if btnLike.titleLabel?.textColor == UIColor(red: 246/255, green: 133/255, blue: 31/255, alpha: 1)
        {
            ref = Database.database().reference().child("tbl_thread_like").child(KnockrSingletonClass.sharedInstance.threadId)

            ref.observe(DataEventType.value, with: { (snapshot) in
                let postDict = snapshot.value as? [String : AnyObject] ?? [:]

                print("Data = ",postDict)

                self.arrayLikes.removeAll()
                
                for ( key, value) in postDict
                {
                    let dic = value as? [String : AnyObject]

                    if KnockrSingletonClass.sharedInstance.userID == dic!["user_id"] as! String
                    {
                        FirebaseDatabase.Database.database().reference(withPath: "tbl_thread_like").child(KnockrSingletonClass.sharedInstance.threadId).child(key).removeValue()
                        
                        self.btnLike.setTitle(" " + String(self.arrayLikes.count) + " " + "Likes", for: UIControl.State.normal)
                        
                        self.btnLike.setTitleColor(UIColor.darkGray, for: UIControl.State.normal)
                        
                        
                    }
                    else
                    {
                        self.arrayLikes.append(dic! as! [String : String])
                        
                        print("Like Data =",self.arrayLikes)
                        
                        self.btnLike.setTitle(" " + String(self.arrayLikes.count) + " " + "Likes", for: UIControl.State.normal)
                        
                        self.btnLike.setTitleColor(UIColor.darkGray, for: UIControl.State.normal)
                    }
                }
                
            })
            
            self.removeCount()
            
            self.btnLike.setTitle(" " + String(self.arrayLikes.count) + " " + "Likes", for: UIControl.State.normal)
            
            self.btnLike.setTitleColor(UIColor.darkGray, for: UIControl.State.normal)
        }
        else
        {
            let reference = Database.database().reference().child("tbl_thread_like").child(KnockrSingletonClass.sharedInstance.threadId).childByAutoId()
            
            let threadData = [
                "user_id": KnockrSingletonClass.sharedInstance.userID,
                "thread_like_id": reference.key as Any,
                "thread_id": KnockrSingletonClass.sharedInstance.threadId,
                "user_name": KnockrSingletonClass.sharedInstance.name] as [String : Any]
            
            reference.setValue(threadData)
            
            ref = Database.database().reference().child("tbl_thread_like").child(KnockrSingletonClass.sharedInstance.threadId)
            
            ref.observe(DataEventType.value, with: { (snapshot) in
                let postDict = snapshot.value as? [String : AnyObject] ?? [:]
                
                print("Data = ",postDict)
                
                self.arrayLikes.removeAll()
                
                for ( _, value) in postDict
                {
                    let dic = value as? [String : AnyObject]
                    
                    self.arrayLikes.append(dic! as! [String : String])
                    
                    self.btnLike.setTitle(" " + String(self.arrayLikes.count) + " " + "Likes", for: UIControl.State.normal)
                    
                    self.btnLike.setTitleColor(UIColor(red: 246/255, green: 133/255, blue: 31/255, alpha: 1), for: UIControl.State.normal)
                }
                
            })
            
            data()
        }
    }
    
    
    @IBAction func btnComment(_ sender: Any)
    {
    
    }
    
    @IBAction func btnThreadComment(_ sender: Any)
    {
        let reference = Database.database().reference().child("tbl_thread_comment").child(KnockrSingletonClass.sharedInstance.threadId).childByAutoId()
        
        let threadData = [
            "user_id": KnockrSingletonClass.sharedInstance.userID,
            "thread_comment": txtViewThreadComment.text,
            "thread_comment_id": reference.key as Any,
            "thread_id": KnockrSingletonClass.sharedInstance.threadId,
            "user_name": KnockrSingletonClass.sharedInstance.name] as [String : Any]
        
        reference.setValue(threadData)
        
        txtViewThreadComment.text = "Write a Comment..."
        txtViewThreadComment.textColor = UIColor.lightGray
        
        txtViewThreadComment.resignFirstResponder()
        
        data()
        
        viewWillAppear(true)
    }
    
    func data()
    {
        ref = Database.database().reference().child("tbl_thread").child(KnockrSingletonClass.sharedInstance.threadId)
        
        ref.observe(DataEventType.value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            
            print("Data1 = ",postDict)
            
            self.noOfCommentLike = postDict["no_of_like_comment"] as! Int
            
            print("No of Count = ",self.noOfCommentLike as Any)
        })
        
        self.addCount = noOfCommentLike + 1
        
        print("Total count = ",self.addCount)
        
        self.ref.updateChildValues(["no_of_like_comment": self.addCount])
    }
    
    func removeCount()
    {
        ref = Database.database().reference().child("tbl_thread").child(KnockrSingletonClass.sharedInstance.threadId)
        
        ref.observe(DataEventType.value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            
            print("Data1 = ",postDict)
            
            self.noOfCommentLike = postDict["no_of_like_comment"] as! Int
            
            print("No of Count = ",self.noOfCommentLike as Any)
        })
        
        self.addCount = noOfCommentLike - 1
        
        print("Total count = ",self.addCount)
        
        self.ref.updateChildValues(["no_of_like_comment": self.addCount])
    }
    
    @IBAction func btnLikeToComment(_ sender: Any)
    {
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: tblThradView)
        let indexPath = tblThradView.indexPathForRow(at:buttonPosition)
        
        let array = arrayComments[indexPath!.row]
        let comment_id = array["thread_comment_id"]
        
        if btnLike.titleLabel?.textColor == UIColor(red: 246/255, green: 133/255, blue: 31/255, alpha: 1)
        {
            ref = Database.database().reference().child("tbl_thread_like").child(KnockrSingletonClass.sharedInstance.threadId)
            
            ref.observe(DataEventType.value, with: { (snapshot) in
                let postDict = snapshot.value as? [String : AnyObject] ?? [:]
                
                print("Data = ",postDict)
                
                self.arrayLikes.removeAll()
                
                for ( key, value) in postDict
                {
                    let dic = value as? [String : AnyObject]
                    
                    if KnockrSingletonClass.sharedInstance.userID == dic!["user_id"] as! String
                    {
                        FirebaseDatabase.Database.database().reference(withPath: "tbl_thread_like").child(KnockrSingletonClass.sharedInstance.threadId).child(key).removeValue()
                        
                        self.btnLike.setTitle(" " + String(self.arrayLikes.count) + " " + "Likes", for: UIControl.State.normal)
                        
                        self.btnLike.setTitleColor(UIColor.darkGray, for: UIControl.State.normal)
                    }
                    else
                    {
                        self.arrayLikes.append(dic! as! [String : String])
                        
                        print("Like Data =",self.arrayLikes)
                        
                        self.btnLike.setTitle(" " + String(self.arrayLikes.count) + " " + "Likes", for: UIControl.State.normal)
                        
                        self.btnLike.setTitleColor(UIColor.darkGray, for: UIControl.State.normal)
                    }
                }
                
            })
            
            self.btnLike.setTitle(" " + String(self.arrayLikes.count) + " " + "Likes", for: UIControl.State.normal)
            
            self.btnLike.setTitleColor(UIColor.darkGray, for: UIControl.State.normal)
        }
        else
        {
            let reference = Database.database().reference().child("tbl_comment_like").child(KnockrSingletonClass.sharedInstance.threadId).child(comment_id!).childByAutoId()
            
            let threadData = [
                "user_id": KnockrSingletonClass.sharedInstance.userID,
                "comment_like_id": reference.key as Any,
                "comment_id": comment_id as Any,
                "thread_id": KnockrSingletonClass.sharedInstance.threadId,
                "user_name": KnockrSingletonClass.sharedInstance.name] as [String : Any]
            
            reference.setValue(threadData)
            
            ref = Database.database().reference().child("tbl_comment_like").child(KnockrSingletonClass.sharedInstance.threadId).child(comment_id!)
            
            ref.observe(DataEventType.value, with: { (snapshot) in
                let postDict = snapshot.value as? [String : AnyObject] ?? [:]
                
                print("Data = ",postDict)
                
                self.arrayLikes.removeAll()
                
                for ( _, value) in postDict
                {
                    let dic = value as? [String : AnyObject]
                    
                    self.arrayLikes.append(dic! as! [String : String])
                    
                    self.btnLike.setTitle(" " + String(self.arrayLikes.count) + " " + "Likes", for: UIControl.State.normal)
                    
                    self.btnLike.setTitleColor(UIColor(red: 246/255, green: 133/255, blue: 31/255, alpha: 1), for: UIControl.State.normal)
                }
                
            })
            
            
        }
    }
    
    
    @IBAction func btnReplyToComment(_ sender: Any)
    {
        
    }
    
    
    @IBAction func btnBack(_ sender: Any)
    {
        let viewcontrollers = self.navigationController?.viewControllers
        
        viewcontrollers?.forEach({ (vc) in
            if  let inventoryListVC = vc as? ViewController {
                self.navigationController!.popToViewController(inventoryListVC, animated: true)
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
