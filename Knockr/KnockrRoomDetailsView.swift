//
//  KnockrRoomDetailsView.swift
//  Knockr
//
//  Created by Mac on 16/04/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class KnockrRoomDetailsView: UITabBarController,UITabBarControllerDelegate {

    @IBOutlet weak var knockrTabBar: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        knockrTabBar.delegate = self
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        // If your view controller is emedded in a UINavigationController you will need to check if it's a UINavigationController and check that the root view controller is your desired controller (or subclass the navigation controller)
        if tabBarController.selectedIndex == 2 {
        if viewController is YouView {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let controller = storyboard.instantiateViewController(withIdentifier: "YouView") as? YouView {
                controller.modalPresentationStyle = .fullScreen
                self.present(controller, animated: true, completion: nil)
            }
            
            return false
        }
        }
        // Tells the tab bar to select other view controller as normal
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        // here, you should edit "0" to be matched with your selected item
        // for instance, if there is 5 items and the desired item is in the middle, the compared value should be "2"
        if tabBarController.selectedIndex == 2 {
            
            let knockrRoomView = Bundle.main.loadNibNamed("StartNewThreadView1", owner: nil, options: nil)![0] as! StartNewThreadView1
            
            
            self.view.addSubview(knockrRoomView)
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
