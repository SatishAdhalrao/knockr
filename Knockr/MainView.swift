//
//  MainView.swift
//  Knockr
//
//  Created by Mac on 19/03/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import Alamofire
import Gloss
import SwiftyJSON
import NVActivityIndicatorView

extension UIView {
    
    func addShadowView(width:CGFloat=0.2, height:CGFloat=0.2, Opacidade:Float=0.5, maskToBounds:Bool=false, radius:CGFloat=1.5){
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: width, height: height)
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = Opacidade
        self.layer.masksToBounds = maskToBounds
    }
}

class MainView: UIViewController,GIDSignInUIDelegate,FBSDKLoginButtonDelegate,GIDSignInDelegate,NVActivityIndicatorViewable
{
    
    var ref: DatabaseReference!
    
    var alert = UIAlertController()
    
    let  activityIndicatorView = NVActivityIndicatorView(frame:CGRect(x: (UIScreen.main.bounds.width/2)-25, y: (UIScreen.main.bounds.height/2)-25, width: 50, height: 50)
        , type: .ballRotateChase, color: UIColor(red: 103/255, green: 53/255, blue: 145/255, alpha: 1), padding: nil)

   
    @IBOutlet weak var signInButton: GIDSignInButton!
    
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var facbookLogin: UIButton!

    
  
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        ref = Database.database().reference()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "header-bg"), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        facbookLogin.layer.cornerRadius = 2
        facbookLogin.clipsToBounds = true
        facbookLogin.addShadowView()
        
        googleButton.layer.cornerRadius = 2
        googleButton.clipsToBounds = true
        googleButton.addShadowView()
    }
    

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func signInButton(_ sender: Any)
    {
        print("aya")
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func btnLogin(_ sender: Any)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController!.pushViewController(secondViewController, animated: true)
    }
    
    
    @IBAction func btnSignUp(_ sender: Any)
    {
        let signupVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController!.pushViewController(signupVC, animated: true)
    }
    
    
    @IBAction func facbookLogin(_ sender: UIButton)
    {
        let fbLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            if let error = error
            {
                print("Failed to login: \(error.localizedDescription)")
                return
            }
            
            guard let accessToken = FBSDKAccessToken.current() else {
                print("Failed to get access token")
                return
            }
            
            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
            
            // Perform login by calling Firebase APIs
            Auth.auth().signIn(with: credential, completion: { (user, error) in
            
                if let error = error {
                    print("Login error: \(error.localizedDescription)")
                    let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
                    let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(okayAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    return
                }
                
                if let currentUser = Auth.auth().currentUser
                {
                    var userName = ""
                    userName = currentUser.displayName!
                    print("User Name:  \(String(describing: userName))")
                    
                    var userEmail = ""
                    userEmail = currentUser.email!
                    print("User Email:  \(String(describing: userEmail))")
                    
                    let phoneNumber = currentUser.phoneNumber
                    print("User Phone Number:  \(String(describing: phoneNumber))")
                    
                    let userID = currentUser.uid
                    print("User ID:  \(userID)")
                    
                    let userImageURL = currentUser.photoURL!as NSURL
                    print("User Image URL:  \(String(describing: userImageURL))")
                    
                    
                    self.view.addSubview(self.activityIndicatorView)
                    self.activityIndicatorView.startAnimating()
                    
                    let urlString = URL(string: "http://innolytic.guru/knockr/Webs/"+"f_g_login?")
                    
                    let params: [String: Any] = ["username":userEmail as Any, "display_name":userName as Any, "f_g_image_path":userImageURL as Any, "uid":userID, "is_google_user": 0, "is_fb_user": 1]
                    
                    print(params)
                    
                    Alamofire.request(urlString!, method: .post, parameters: params,  headers: nil).responseJSON { (response) in
                        
                        
                        guard (response.error == nil) else
                        {
                            self.activityIndicatorView.stopAnimating()
                            let alertController = UIAlertController(title: "Error", message: response.error!.localizedDescription , preferredStyle: .alert)
                            let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                                
                            }
                            alertController.addAction(action1)
                            self.present(alertController, animated: true, completion: nil)
                            
                            return
                        }
                        
                        if let responseData = response.data
                        {
                            do
                            {
                                let myjson = try JSON(data: responseData)
                                print(myjson)
                                
                                let errorcode = myjson["success"]
                                print(errorcode)
                                if errorcode == "New User registration successfully done."
                                {
                                    self.activityIndicatorView.stopAnimating()
                                    
                                    
//                                    let alertController = UIAlertController(title: "User registration successfully done.", message: "", preferredStyle: .alert)
//                                    let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
//                                        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                                        self.navigationController!.pushViewController(secondViewController, animated: true)
//                                    }
//                                    alertController.addAction(action1)
//                                    self.present(alertController, animated: true, completion: nil)
                                    
                                    // Send User Data To Firebase
                                    
                                    let jsonDict = response.result.value as? [String:Any]
                                    
                                    let dictUserData = jsonDict!["user_data"] as? [String:Any]
                                    
                                    
                                    
                                    KnockrSingletonClass.sharedInstance.loginUsing = "facebookLogin"
                                    UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.loginUsing, forKey: "loginUsing")
                                    
                                    
                                    let userID = dictUserData!["user_id"] as? String
                                    print(userID as Any)
                                    
                                    KnockrSingletonClass.sharedInstance.userID = userID!
                                    
                                    UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.userID, forKey: "userID")
                                    
                                    let userName = dictUserData!["username"] as? String
                                    print(userName as Any)
                                    
                                    KnockrSingletonClass.sharedInstance.userName = userName!
                                    
                                    UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.userName, forKey: "userName")
                                    
                                    
                                    let name =  dictUserData!["display_name"] as? String
                                    print(name as Any)
                                    
                                    KnockrSingletonClass.sharedInstance.name = name!
                                    UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.name, forKey: "Name")
                                    
                                    let profileUrl = dictUserData!["profile_picture"]as? String
                                    
                                    KnockrSingletonClass.sharedInstance.profileUrl = profileUrl!
                                    UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.profileUrl, forKey: "profileUrl")
                                    
                                    
                                    UserDefaults.standard.synchronize()
                                self.ref.child("tbl_user").child(userID!).updateChildValues(dictUserData!)
                                    
                                    
                                   let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    self.navigationController!.pushViewController(secondViewController, animated: true)
                                    
                                }
                                else
                                {
                                    self.alerttoshow(msg: "Somthing Went Wrong")
                                }
                                
                            }
                            catch
                            {
                                self.activityIndicatorView.stopAnimating()
                                self.alerttoshow(msg: "Server issue")
                                print(error)
                                
                            }
                            
                        }
                    }
                }
                
                
                // Present the main view
//                if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") {
////                    UIApplication.shared.keyWindow?.rootViewController = viewController
////                    self.dismiss(animated: true, completion: nil)
//
////                    let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                    self.navigationController!.pushViewController(viewController, animated: true)
//                }
                
            })
            
        }
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let googleAuthentication = GIDSignIn.sharedInstance().handle(url, sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: [:])
        
        
        return googleAuthentication
    }
    
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
//        if let authentication = user.authentication
//        {
        if error == nil
        {
            let credential = GoogleAuthProvider.credential(withIDToken: user.authentication.idToken, accessToken: user.authentication.accessToken)

            Auth.auth().signIn(with: credential, completion: { (user, error) -> Void in
                if error != nil
                {
                    print("Problem at signing in with google with error : \(error)")
                }
                else if error == nil
                {
                    print("user successfully signed in through GOOGLE! uid:\(Auth.auth().currentUser!.uid)")
                    print("signed in")
//                    performSegue(withIdentifier: "goToFeed", sender: self)
                    
                    if let currentUser = Auth.auth().currentUser
                    {
                        var userName = ""
                        userName = currentUser.displayName!
                        print("User Name:  \(String(describing: userName))")
                        
                        var userEmail = ""
                        userEmail = currentUser.email!
                        print("User Email:  \(String(describing: userEmail))")
                        
                        let phoneNumber = currentUser.phoneNumber
                        print("User Phone Number:  \(String(describing: phoneNumber))")
                        
                        let userID = currentUser.uid
                        print("User ID:  \(userID)")
                    
                        let userImageURL = currentUser.photoURL!as NSURL
                        print("User Image URL:  \(String(describing: userImageURL))")
                        
                        self.view.addSubview(self.activityIndicatorView)
                        self.activityIndicatorView.startAnimating()
                        
                        let urlString = URL(string: "http://innolytic.guru/knockr/Webs/"+"f_g_login?")
                        
                        let params: [String: Any] = ["username":userEmail as Any, "display_name":userName as Any, "f_g_image_path":userImageURL as Any, "uid":userID, "is_google_user": 1, "is_fb_user": 0]
                        
                        print(params)
                        
                        Alamofire.request(urlString!, method: .post, parameters: params,  headers: nil).responseJSON { (response) in
                            
                            
                            guard (response.error == nil) else
                            {
                                self.activityIndicatorView.stopAnimating()
                                let alertController = UIAlertController(title: "Error", message: response.error!.localizedDescription , preferredStyle: .alert)
                                let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                                    
                                }
                                alertController.addAction(action1)
                                self.present(alertController, animated: true, completion: nil)
                                
                                return
                            }
                            
                            if let responseData = response.data
                            {
                                do
                                {
                                    let myjson = try JSON(data: responseData)
                                    print(myjson)
                                    
                                    let errorcode = myjson["success"]
                                    print(errorcode)
                                    if errorcode == "New User registration successfully done."
                                    {
                                        self.activityIndicatorView.stopAnimating()
                                        
                                        
                                        //                                    let alertController = UIAlertController(title: "User registration successfully done.", message: "", preferredStyle: .alert)
                                        //                                    let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                                        //                                        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                        //                                        self.navigationController!.pushViewController(secondViewController, animated: true)
                                        //                                    }
                                        //                                    alertController.addAction(action1)
                                        //                                    self.present(alertController, animated: true, completion: nil)
                                        
                                        // Send User Data To Firebase
                                        
                                        let jsonDict = response.result.value as? [String:Any]
                                        
                                        let dictUserData = jsonDict!["user_data"] as? [String:Any]
                                        
                                        
                                        
                                        KnockrSingletonClass.sharedInstance.loginUsing = "googleLogin"
                                        UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.loginUsing, forKey: "loginUsing")
                                        
                                        let userID = dictUserData!["user_id"] as? String
                                        print(userID as Any)
                                        
                                        KnockrSingletonClass.sharedInstance.userID = userID!
                                        
                                        UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.userID, forKey: "userID")
                                        
                                        let userName = dictUserData!["username"] as? String
                                        print(userName as Any)
                                        
                                        KnockrSingletonClass.sharedInstance.userName = userName!
                                        
                                        UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.userName, forKey: "userName")
                                        
                                        
                                        let name =  dictUserData!["display_name"] as? String
                                        print(name as Any)
                                        
                                        KnockrSingletonClass.sharedInstance.name = name!
                                        UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.name, forKey: "Name")
                                        
                                        let profileUrl = dictUserData!["profile_picture"] as? String
                                        
                                        KnockrSingletonClass.sharedInstance.profileUrl = profileUrl!
                                        UserDefaults.standard.setValue(KnockrSingletonClass.sharedInstance.profileUrl, forKey: "profileUrl")
                                        
                                        
                                        UserDefaults.standard.synchronize()
                                        self.ref.child("tbl_user").child(userID!).updateChildValues(dictUserData!)
                                        
                                        
                                        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                        self.navigationController!.pushViewController(secondViewController, animated: true)
                                        
                                    }
                                    else
                                    {
                                        self.alerttoshow(msg: "Somthing Went Wrong")
                                    }
                                    
                                }
                                catch
                                {
                                    self.activityIndicatorView.stopAnimating()
                                    self.alerttoshow(msg: "Server issue")
                                    print(error)
                                    
                                }
                                
                            }
                        }
                    }
                }
            })
        }
        
    }
    
    
    // pressed the Sign In button
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }

    
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!)
    {
        
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!)
    {
        
    }
    

   
    
    func alerttoshow(msg:String)
    {
        alert = UIAlertController (title: msg, message: nil, preferredStyle: UIAlertController.Style.alert)
        perform(#selector(dismissalert), with: nil, afterDelay: 2)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func dismissalert()
    {
        self.activityIndicatorView.stopAnimating()
        
        alert.dismiss(animated: true, completion: nil)
    }
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
