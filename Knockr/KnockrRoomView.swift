//
//  KnockrRoomView.swift
//  Knockr
//
//  Created by Bhargav Mavani on 10/04/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage
import SKPhotoBrowser
import CoreLocation
import AVKit

extension UIView {
    
    func shadow(color: UIColor = UIColor.darkGray) {
        self.layer.shadowColor = color.cgColor;
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 1
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
    }
}

extension UIView {
    
    func setShadowWithCornerRadius(corners : CGFloat){
        
        self.layer.cornerRadius = corners
        
        let shadowPath2 = UIBezierPath(rect: self.bounds)
        
        self.layer.masksToBounds = false
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        
        self.layer.shadowOffset = CGSize(width: CGFloat(2.0), height: CGFloat(2.0))
        
        self.layer.shadowOpacity = 0.7
        
        self.layer.shadowPath = shadowPath2.cgPath
        
        self.layer.shadowRadius = 10.0
        
    }
}

class KnockrRoomView: UIView {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var roomNameLabel: UILabel!
    @IBOutlet weak var roomDescriptionLabel: UILabel!
    @IBOutlet weak var activeUserLabel: UILabel!
    @IBOutlet var ConstraintButtonHeight: NSLayoutConstraint!
    @IBOutlet var ConstraintButtonWidth: NSLayoutConstraint!
    @IBOutlet var knockButton: UIButton!

    private var shadowLayer: CAShapeLayer!
    var KnockrDetail = [String : AnyObject]()
    var roomDetailArray = NSMutableArray()
    var images = [SKPhoto]()

    override func awakeFromNib() {
        
        adjustScreenSize()
        self.perform(#selector(getRoomDetail), with: nil, afterDelay: 0.1)
    }
    
    func adjustScreenSize() {
        
        if UIScreen.main.bounds.height <= 568 {
            ConstraintButtonWidth.constant = 130
            ConstraintButtonHeight.constant = 30
            knockButton.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 11.0)
        }
        
        self.frame = CGRect(x: 10, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height+15)
        mainView.setShadowWithCornerRadius(corners: 12)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.frame = CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height+15)
        }) { (bool) in
            
        }
    }
    
    @IBAction func removeSelfWithAnimation(sender : UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.frame = CGRect(x: 10, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.height+15)
        }) { (bool) in
            self.removeFromSuperview()
        }
    }
    
    @objc func getRoomDetail() {
        
        roomNameLabel.text = KnockrDetail["room_name"] as? String ?? ""
        roomDescriptionLabel.text = KnockrDetail["description"] as? String ?? ""

        let room_id = KnockrDetail["room_id"] as? Int ?? 0
        
        var ref: DatabaseReference!
        
        ref = Database.database().reference().child("tbl_room_image").child("\(room_id)")
        
        ref.observe(DataEventType.value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]

            for (key, value) in postDict {
                print("\(key) -> \(value)")
                if let dic = value as? [String : AnyObject] {
                    self.roomDetailArray.add(dic)
                }
            }
            self.getRoomVideo()
        })
    
        var activeUserCount = 0
        
        var latitude = 0.0
        var longitude = 0.0
        
        if let latitudeString = KnockrDetail["latitude"] as? String {
            latitude = Double(latitudeString) ?? 0.0
        }
        
        if let longitudeString = KnockrDetail["longitude"] as? String {
            longitude = Double(longitudeString) ?? 0.0
        }
        
        let roomLocation: CLLocation = CLLocation(latitude:latitude, longitude: longitude)

        ref = Database.database().reference().child("tbl_user_location")
        
        ref.observe(DataEventType.value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            
            for (key, value) in postDict {
                print("\(key) -> \(value)")
                if let dic = value as? [String : AnyObject] {
                    
                    var Userlatitude = 0.0
                    var Userlongitude = 0.0

                    if let latitudeString = dic["latitude"] as? String {
                        Userlatitude = Double(latitudeString) ?? 0.0
                    }
                    
                    if let longitudeString = dic["longitude"] as? String {
                        Userlongitude = Double(longitudeString) ?? 0.0
                    }

                    let userLocation: CLLocation = CLLocation(latitude:Userlatitude, longitude: Userlongitude)

                    let distanceInMeters = roomLocation.distance(from: userLocation) // result is in meters
                    
                    if distanceInMeters <= 100 {
                        activeUserCount = activeUserCount + 1
                    }
                }
            }
            self.activeUserLabel.text = "\(activeUserCount)"
            
            KnockrSingletonClass.sharedInstance.activeUser = "\(activeUserCount)"
        })
    }
    
    func getRoomVideo() {
        let room_id = KnockrDetail["room_id"] as? Int ?? 0
        
        var ref: DatabaseReference!
        
        ref = Database.database().reference().child("tbl_room_vedio").child("\(room_id)")
        
        ref.observe(DataEventType.value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            
            for (key, value) in postDict {
                print("\(key) -> \(value)")
                if let dic = value as? [String : AnyObject] {
                    let dictInfo = NSMutableDictionary(dictionary: dic)
                    dictInfo.setValue("1", forKey: "isVideo")
                    self.roomDetailArray.add(dictInfo)
                }
            }
            self.setUpImageScroll()
        })
    }
    
    func setUpImageScroll() {
    
        var xPos = 0.0
        let yPos = 0.0
        for index in 0..<roomDetailArray.count
            {
               let dictionary = roomDetailArray.object(at: index)
                
                if let dict = dictionary as? NSDictionary {
                
                    let roomImageView = UIImageView(frame: CGRect(x: xPos, y: yPos, width: 100, height: 50))

                    var url = ""
                    if let isVideo = dict.value(forKey: "isVideo") as? String,isVideo == "1" {
                        url = "http://innolytic.guru/knockr/\(dict.value(forKey: "room_vedio_path") as? String ?? "")"
                        DispatchQueue.main.async {
                        roomImageView.image = self.getThumbnailImage(forUrl: URL(string: url)!)
                        }
                    }
                    else {
                        url = "http://innolytic.guru/knockr/\(dict.value(forKey: "room_image_path") as? String ?? "")"
                        roomImageView.sd_setImage(with: URL(string: url)) { (image, error, type, url) in
                        }
                    }
                
                roomImageView.contentMode = .scaleAspectFill
                roomImageView.clipsToBounds = true
                roomImageView.layer.cornerRadius = 2
                roomImageView.layer.masksToBounds = true
                roomImageView.tag = index
                imageScrollView.addSubview(roomImageView)
                roomImageView.isUserInteractionEnabled = true
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapOnImage(gesture:)))
                roomImageView.addGestureRecognizer(tapGesture)
                
                xPos = xPos + Double(roomImageView.frame.size.width) + 10.0
                
                    if let isVideo = dict.value(forKey: "isVideo") as? String {
                        if isVideo == "1" {
                            let playImageView = UIImageView(frame: CGRect(x: xPos, y: yPos, width: 25, height: 25))
                            playImageView.image = UIImage(named: "play")
                            playImageView.center = roomImageView.center
                            imageScrollView.addSubview(playImageView)
                        }
                    }
                    else {
                        let photo = SKPhoto.photoWithImageURL(url) // add some UIImage
                        images.append(photo)
                    }
                }
            }
        imageScrollView.contentSize = CGSize(width: xPos, height: 50)
    }
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
            let asset = AVAsset(url: url)
            let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            assetImgGenerate.appliesPreferredTrackTransform = true
            let time = CMTimeMake(value: 1, timescale: 60)
            let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            if let image = img,img != nil {
                return UIImage(cgImage:image)
            }
            return nil
    }

    @objc func tapOnImage(gesture : UITapGestureRecognizer) {
        
        let myApp = UIApplication.shared.delegate as! AppDelegate
        
        let index = gesture.view?.tag ?? 0
        
        if let dict = roomDetailArray.object(at: index) as? NSDictionary {
            if let isVideo = dict.value(forKey: "isVideo") as? String, isVideo == "1" {
                let url = "http://innolytic.guru/knockr/\(dict.value(forKey: "room_vedio_path") as? String ?? "")"
                let player = AVPlayer(url: URL(string: url)!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                myApp.window!.rootViewController?.present(playerViewController, animated: true, completion: {
                    playerViewController.player!.play()
                })
                return
            }
        }
        
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(index)
        myApp.window!.rootViewController?.present(browser, animated: true, completion: nil)
    }
    
    
    @IBAction func btnKnockTheDoor(_ sender: Any)
    {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "KnockTheDoor"), object: nil)
    }
}
